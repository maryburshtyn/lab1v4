package lab1V4;
//�������� �� ����(������� �������)
//���������� ������ ����
//������� � 2-3 ����:
//1)��� ����. ����� ���������� �� ����������
//2) 3 ������ ���
//3)��� ����� ������������/ �����������/ ������������
//4) ���� �� � Java ������������� ������������? (���)
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

/** The class is used to create an
 * application that determines which triangle
 * rectangular, equilateral or isosceles.
 * @author Mary Burshtyn
 * @version 1.0
 */
public class CheckingUpTriangle extends Application {
/**HEIGHT constant for specifying the height of the window.
 */
    private static final double HEIGHT = 350;
/**WIDTH constant for specifying the width of the window.
*/
    private static final double WIDTH = 160;
/**WIDTH_OF_TEXTFIELD constant for specifying the width of the text field.
*/
    private static final double WIDTH_OF_TEXTFIELD = 220;
/**SPACING constant for spacing.
 */
    private static final double SPACING = 5;
    /** A helper class for checking sides and parsing.
     *@throws NumberFormatException an exception is
     *thrown out beyond the allowed boundaries
     */
    class SupportingClass {
        /**
         * @param side1 - numeric value of side 1
         * @param side2 - numeric value of side 2
         * @param side3 - numeric value of side 3
         * @return true - if the entered side is
         * a positive number, false - if 0
         */
        boolean checkZero(final int side1,
                final int side2, final int side3) {
            return (side1 > 0 && side2 > 0 && side3 > 0);
        }
        /**
         * @param side1 - numeric value of side 1
         * @param side2 - numeric value of side 2
         * @param side3 - numeric value of side 3
         * @return true -  if equilateral
         * triangle, false - if not.
         */
        boolean checkEquilateral(final int side1,
                final int side2, final int side3) {
            return (side1 == side2 && side2 == side3);
        }
        /**
         * @param side1 - numeric value of side 1
         * @param side2 - numeric value of side 2
         * @param side3 - numeric value of side 3
         * @return true - if isosceles triangle, false - if not
         */
        boolean checkIsosceles(final int side1,
                final int side2, final int side3) {
            return (side1 == side2 || side2 == side3
                    || side1 == side3);
        }
        /**
         * @param side1 - numeric value of side 1
         * @param side2 - numeric value of side 2
         * @param side3 - numeric value of side 3
         * @return true - if the triangle is
         * rectangular, false - if not
         */
        boolean ckeckRight(final int side1,
                final int side2, final int side3) {
            return (side1 * side1 + side2 * side2 == side3 * side3
                    || side1 * side1 + side3 * side3 == side2 * side2
                    || side2 * side2 + side3 * side3 == side1 * side1);
        }
        /**
         * @param side1 - numeric value of side 1
         * @param side2 - numeric value of side 2
         * @param side3 - numeric value of side 3
         * @return true - if entered numbers,
         * false - if you typed the characters
         */
        boolean checkForNumbers(final String side1,
                final String side2, final String side3) {
            return (side1.matches("[0-9]*")
                && side2.matches("[0-9]*") && side3.matches("[0-9]*"));
        }
        /**
         *
         * @param s - string for parsing
         * @return returns an Integer object with
         * the value of the side of the triangle
         * @throws NumberFormatException
         * an exception is thrown out beyond the allowed boundaries
         */
        Integer parseSide(final String s) {
            try {
                return Integer.parseInt(s);
            } catch (NumberFormatException ex) {
                System.out.print("Exeption");
                return 0;
            }
        }
    }
    /**The entry point to the class and the application.
     * @param args Array of String Arguments
     */
    public static void main(final String[] args) {
    Application.launch(args);
    }
    //��������������� ������ Start()
    /**Method which run program.
     */
    @Override
    public void start(final Stage mainStage) {
        mainStage.setTitle("�������� ������������");
        mainStage.setResizable(false);
        AnchorPane anchorPane = new AnchorPane();
        Scene scene = new Scene(anchorPane, HEIGHT, WIDTH);
        GridPane gridpane = new GridPane();

        Label side = new Label("������� 1");
        TextField side1TextField = new TextField();
        side1TextField.setPrefWidth(WIDTH_OF_TEXTFIELD);

        HBox hb1 = new HBox();
        hb1.setSpacing(SPACING);
        hb1.getChildren().addAll(side, side1TextField);
        gridpane.add(hb1, 0, 0);

        side = new Label("������� 2");
        TextField side2TextField = new TextField();
        side2TextField.setPrefWidth(WIDTH_OF_TEXTFIELD);

        HBox hb2 = new HBox();
        hb2.setSpacing(SPACING);
        hb2.getChildren().addAll(side, side2TextField);
        gridpane.add(hb2, 0, 1);

        side = new Label("������� 3");
        TextField side3TextField = new TextField();
        side3TextField.setPrefWidth(WIDTH_OF_TEXTFIELD);

        HBox hb3 = new HBox();
        hb3.setSpacing(SPACING);
        hb3.getChildren().addAll(side, side3TextField);
        gridpane.add(hb3, 0, 2);

        GridPane gridpane2 = new GridPane();

        Label equilateralTriangle = new Label("��������������: ");
        gridpane2.add(equilateralTriangle, 0, 0);
        Label isoscelesTriangle = new Label("��������������: ");
        gridpane2.add(isoscelesTriangle, 0, 1);
        Label rightTriangle = new Label("�������������: ");
        gridpane2.add(rightTriangle, 0, 2);

        anchorPane.setTopAnchor(gridpane, 10.0);
        anchorPane.setLeftAnchor(gridpane, 10.0);
        anchorPane.setBottomAnchor(gridpane2, 10.0);
        anchorPane.setLeftAnchor(gridpane2, 10.0);

        Button checkupButton = new Button("���������");
        anchorPane.setBottomAnchor(checkupButton, 10.0);
        anchorPane.setRightAnchor(checkupButton, 20.0);
        
        Button clearButton = new Button("�������");
        anchorPane.setBottomAnchor(clearButton, 10.0);
        anchorPane.setRightAnchor(clearButton, 100.0);

        /*Label bsuirLabel = new Label("����� - ������ � ����� �����!");
        anchorPane.setBottomAnchor(bsuirLabel, 40.0);
        anchorPane.setRightAnchor(bsuirLabel, 20.0);
        bsuirLabel.setVisible(false);*/
        clearButton.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(final ActionEvent e) {
            	side1TextField.clear();
            	side2TextField.clear();
            	side3TextField.clear();
            }
            
            });
      //������� ���������� ������
        checkupButton.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(final ActionEvent ex) {
                //bsuirLabel.setVisible(true);
                SupportingClass support = new SupportingClass();

                if (side1TextField.getText().isEmpty()
                        || side2TextField.getText().isEmpty()
                        || side3TextField.getText().isEmpty()) {
                    equilateralTriangle.setText("��������������:"
                            + " �� �� ����� ������� ���y��������");
                    return;
                }
                if (support.checkForNumbers(side1TextField.getText(),
                    side2TextField.getText(), side3TextField.getText())) {

                    Integer side1 = support.parseSide(side1TextField.getText());
                    Integer side2 = support.parseSide(side2TextField.getText());
                    Integer side3 = support.parseSide(side3TextField.getText());

                    if (support.checkZero(side1, side2, side3)) {
                        if (support.checkEquilateral(side1, side2, side3)) {
                            equilateralTriangle.setText("��������������: ��");
                        } else {
                        equilateralTriangle.setText("��������������: ���");
                        }
                        if (support.checkIsosceles(side1, side2, side3)) {
                            isoscelesTriangle.setText("��������������: ��");
                        } else {
                            isoscelesTriangle.setText("��������������: ���");
                        }
                        if (support.ckeckRight(side1, side2, side3)) {
                           rightTriangle.setText("�������������: ��");
                        } else {
                            rightTriangle.setText("�������������: ���");
                        }
                    } else {
                        equilateralTriangle.setText("��������������: "
                                + "�� �� ������� ����������� ��������");
                    }
                } else {
                    equilateralTriangle.setText("��������������: "
                            + "�� ����� �� ����� � ��������� ����");
                }
            }
        });

        anchorPane.getChildren().add(gridpane2);
        anchorPane.getChildren().add(gridpane);
        anchorPane.getChildren().add(checkupButton);
        anchorPane.getChildren().add(clearButton);
        //anchorPane.getChildren().add(bsuirLabel);
        mainStage.setScene(scene);
        mainStage.show();
    }
}
